#include "DHT.h"


#define DHTPIN 5
#define DHTTYPE DHT11

#define INFLUXDB_HOST "10.10.10.11"
#define INFLUXDB_PORT 8086
#define INFLUXDB_DATABASE "iot"

#define INFLUXDB_MEASUREMENT_HUMIDITY_NAME "humidity"
#define INFLUXDB_MEASUREMENT_TEMPERATURE_NAME "temperature"
#define INFLUXDB_ROOM_TAG "room1"

#define WIFI_SSID "BestWifi"
#define WIFI_PASSWORD "hunter2"
